let inputText = process.argv[2];
const figlet = require("figlet");
figlet.text(inputText, (error, data) => {
    if (error)
        console.log(error);
    else
        console.log(data);
});
